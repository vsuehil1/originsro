# Welcome to the OriginsRO Bug Tracker

![OriginsRO](.resources/topimage.jpg)

## OriginsRO Main Website and Downloads

If you're just looking for OriginsRO or want to download the client, please head to https://originsro.org/

## Bug and Feature Tracker

If you're looking for **bug reports** or **feature suggestions** concerning OriginsRO, this is the right place!

You can [browse the **existing issues**](https://gitlab.com/originsro/originsro/issues) or [create a **new one**](https://gitlab.com/originsro/originsro/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

If you prefer, you can see the existing issues as a [kanban board](https://gitlab.com/originsro/originsro/boards) or view the [development milestones](https://gitlab.com/originsro/originsro/milestones).

Please note that in order to post new issues, you'll need to [sign in or sign up for a free GitLab account](https://gitlab.com/users/sign_in?redirect_to_referer=yes).

![Thief Bug](.resources/ThiefBug_Female.gif)

## Reporting an Exploit & Bounties

When reporting something exploitive please mark the issue as confidential.

Some exploits of high severity make the reporter eligible for a **cosmetic bounty**, such as:
- Damage Exploits, like 1-hitting everything with ease.
- Zeny Exploits, gaining infinite Zeny with ease.
- Crashing others or the server or making the service unavailable. (Except for DDoS attacks)

**The severity of each bug will be evaluated by the staff**

## Support and Player Reports

If you want to open a **support ticket** or **report a player**, please head to our [Help Desk](https://gitlab.com/originsro/originsro-support)

## Technical Support

If you're looking for **technical support** [Forum](https://bbs.originsro.org/) or [Discord Server](https://originsro.org/discord).

## OriginsRO useful links

- Main website: https://originsro.org
- Forum: https://bbs.originsro.org
- Control Panel: https://cp.originsro.org
- Wiki: https://wiki.originsro.org
- Bug and Feature Tracker: https://gitlab.com/originsro/originsro
- Help Desk: https://gitlab.com/originsro/originsro-support
