# API Docs

The main OriginsRO API is a
[REST](https://en.wikipedia.org/wiki/Representational_state_transfer) API.
Therefore, documentation in this section assumes knowledge of REST concepts.

## Available API resources

For a list of the available resources and their endpoints, see
[API resources](api_resources.md).

## Compatibility guidelines

The HTTP API is versioned using a single number, the current one being 1. This
number symbolizes the same as the major version number as described by
[SemVer](https://semver.org/). This mean that backward incompatible changes
will require this version number to change. However, the minor version is not
explicit. This allows for a stable API endpoint, but also means new features
can be added to the API in the same version number.

New features and bug fixes are released at any time, and are announced through
updates to the documentation in this repository. All deprecations and changes
between two versions will be listed in the documentation, and the previous,
deprecated API versions will be available for a short period of time after a
major update.

### Current status

Currently only API version v1 is available.

## Basic usage

API requests should be prefixed with `api` and the API version. For example,
the root of the v1 API is at `/api/v1`.

Example of a valid API request using cURL:

```shell
curl "https://api.originsro.org/api/v1/test"
```

The API uses JSON to serialize data. You don't need to specify `.json` at the
end of an API URL.

## Authentication

Use of the API requires authentication, unless differently specified in the
endpoints documentation.

If authentication information is invalid or omitted, an error message will be
returned with status code `401`:

```json
{
  "message": "401 Unauthorized"
}
```

You can use an API Key to authenticate with the API by passing it in either the
`api_key` parameter or the `x-api-key` header.

Example of using the API Key in a parameter:

```shell
curl https://api.originsro.org/api/v1/test?api_key=<your_api_key>
```

Example of using the API Key in a header:

```shell
curl --header "x-api-key: <your_api_key>" https://api.originsro.org/api/v1/test
```

API keys are tied to a specific Master Account and should never be shared. API
keys can be requested and revoked through the [Control
Panel](https://cp.originsro.org/masteraccount/view).

## Status codes

The API is designed to return different status codes according to context and
action. This way, if a request results in an error, the caller is able to get
insight into what went wrong.

The following table gives an overview of how the API functions generally behave.

| Request type | Description |
| ------------ | ----------- |
| `GET`   | Access one or more resources and return the result as JSON. |
| `POST`  | Return `201 Created` if the resource is successfully created and return the newly created resource as JSON. |
| `GET` / `PUT` | Return `200 OK` if the resource is accessed or modified successfully. The (modified) result is returned as JSON. |
| `DELETE` | Returns `204 No Content` if the resource was deleted successfully. |

The following table shows the possible return codes for API requests.

| Return values | Description |
| ------------- | ----------- |
| `200 OK` | The `GET`, `PUT` or `DELETE` request was successful, the resource(s) itself is returned as JSON. |
| `204 No Content` | The server has successfully fulfilled the request and that there is no additional content to send in the response payload body. |
| `201 Created` | The `POST` request was successful and the resource is returned as JSON. |
| `304 Not Modified` | Indicates that the resource has not been modified since the last request. |
| `400 Bad Request` | A required attribute of the API request is missing. |
| `401 Unauthorized` | The user is not authenticated, a valid [API Key](#authentication) is necessary. |
| `403 Forbidden` | The request is not allowed for the current user. |
| `404 Not Found` | A resource could not be accessed, e.g., an ID for a resource could not be found. |
| `405 Method Not Allowed` | The request is not supported. |
| `409 Conflict` | A conflicting resource already exists, e.g., creating an entry with a name that already exists. |
| `412` | Indicates the request was denied. May happen if the `If-Unmodified-Since` header is provided when trying to delete a resource, which was modified in between. |
| `422 Unprocessable` | The entity could not be processed. |
| `500 Server Error` | While handling the request something went wrong server-side. |

## Data validation and error reporting

When working with the API you may encounter validation errors, in which case
the API will answer with an HTTP `400` status.

Such errors appear in two cases:

- A required attribute of the API request is missing
- An attribute did not pass the validation

When an attribute is missing, you will get something like:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "message":"400 Bad request: 'field name' not given"
}
```

When a validation error occurs, error messages will be different. They will
hold all details of validation errors:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "message": {
        "field name": [
            "is too long (maximum is 255 characters)"
        ]
    }
}
```

This makes error messages more machine-readable. The format can be described as
follows:

```json
{
    "message": {
        "<property-name>": [
            "<error-message>",
            "<error-message>",
            ...
        ],
        "<embed-entity>": {
            "<property-name>": [
                "<error-message>",
                "<error-message>",
                ...
            ],
        }
    }
}
```

## Unknown route

When you try to access an API URL that does not exist you will receive 404 Not Found.

```
HTTP/1.1 404 Not Found
Content-Type: application/json
{
    "error": "404 Not Found"
}
```

## Encoding `+` in ISO 8601 dates

If you need to include a `+` in a query parameter, you may need to use `%2B` instead due
to a [W3 recommendation](http://www.w3.org/Addressing/URL/4_URI_Recommentations.html) that
causes a `+` to be interpreted as a space. For example, in an ISO 8601 date, you may want to pass
a time in Mountain Standard Time, such as:

```
2017-10-17T23:11:13.000+05:30
```

The correct encoding for the query parameter would be:

```
2017-10-17T23:11:13.000%2B05:30
```

## Rate limits

The use of the OriginsRO API is subject to rate limits, varying for each different endpoint.

The API responds with HTTP status code 429 to API requests that exceed the limit per IP address.

The following example headers are included for all API requests:

```
X-RateLimit-Limit: 12
X-RateLimit-Remaining: 0
X-RateLimit-Reset: 1574533522
Retry-After: 3597
```

In case of frequent abuse of the rate limits, or in case of unusual traffic
from an IP or an API Key, a temporary or permanent ban may be automatically
issued. If you receive a 403 Forbidden error for all requests to
api.originsro.org, please contact the OriginsRO Support with details, such as
the affected IP address and your Master Account.
