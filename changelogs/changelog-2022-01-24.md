## Update Changelog - 2022-01-24 (Ep. 10.4 end of christmas event)

### Changelog for Players

#### Added
- **UI**: Added support for closing `Teleport` or `Warp` window via ESC key like other windows. Related to originsro#2716 [C:8e712b0ffc]

#### Changed
- **Events**: Disabled christmas event. [5f8228544d, 462fc0c8c5]
- **Events**: Improved and fixed stuff in Christmas Event NPCs, this was patched in as hotfix when the event was running. [30e44d0795, 848518b736, 7a5078c81b, fbdcde82b6, 2ad87d1586, f202268728, 08cefbb3e5, 213a257ad0]
- **Misc**: Splitted sideworld teaser from the christmas event. People that completed this part are able to create the headgears there. [977e407245]
- **Misc**: Disabled Halloween Boxes and other items. They aren't useable but can be kept for a future event. [98d6570401]

#### Fixed
- **Skills**: Fixed `Chain Crush Combo` not working after `Raging Thrust`. Related to originsro#1912 [3d66b0f796]
- **Misc**: Fixed some items not getting removed from API and CP listing of shops. [44a9bcdc1b]
- **Skills**: Fixed `Freezing Trap` not working. Related to originsro#3125 [4df593001b]
- **Items**: Fixed the Xmas in the 13th month musicbox. Related to originsro#3123 [a732f69a80]
- **Skills**: Fixed a rare crash in relation to marriage skills. [C:169475eaec]

> This update consists of 32 commits.
