## Update Changelog - 2021-07-03 (Ep. 10.4 Hugel & Odin)

### Changelog for Players

#### Added

- **Mapflags**: Added no-vending areas for streets and harbor in alberta. [95462e7274]
- **Maps**, **Quests**, **NPCs**, **Mobs**, **Items**: Added Hugel and Odin. [e58b368fc7, bc079f2896, 9d54bfe7fd, cf57c593c2, f32ba746fe, C:45de55936f, C:51353087be, C:556f0292c8, 96b5463b4f, C:9f28641659, C:0a3fdb814b]
- **Quests**: Activated the following Hugel Quests: [6a5ee0d637]
  - Medicine Quest
  - Biological Weapon Quest
  - Rebellion Quest
  - Hugel Memory Quest
  - Hide and Seek Quest
  - Fish Cake Soup Delivery Quest
  - Juno Remedy Quest
  - Cow Milking Quest
  - Odin's Temple Excavation Quest
- **NPCs**: Added Hugel Mini Games: Bingo and Monster Races. [a90c19fba3, a3204773ff, 73a4553e49]
- **Quests**: Enabled the Cooking Quest. [975203a703]
- **Items, NPCs**: Added Level 1 to 10 stat food and restored cards that are already available to their original effect. [e0ec733f17, 4da80d280e, e204aab68a, 916b4c1b55, 7261ca3acf, a0ba98ae12, a2a316b0ee, 3c3ae2acc9, b028846a2a, e2f9a192a6, dc023ef905, b670c07b5e, dc63c92987, 591540fb71, C:e35cba4e5a]
- **NPCs**: Enabled weddings in Hugel. [306ec834a9]
- **Mapflags**: Added the possibility to autotrade on Sphinx Dungeon Entrance Map (moc_fild19) and Sunken Ship (alb2trea). Related to originsro/originsro#1497 [769aa87403, 4eb9c96826]
- **Items**: Added 15 new Costumes: [cdfff6c64c, C:245e2fc817]
  - Mini Propeller
  - Gangster Mask
  - Bomb Wick
  - Jewel Crown
  - Oxygen Mask
  - Aerial
  - Grief for Greed
  - Chef Hat
  - Party Hat
  - Decorative Mushroom
  - Poker Face
  - Surprised Mask
  - Annoyed Mask
  - Bucket Hat
  - Mythical Lion Mask
- **NPCs**: Added a warp to Juno to the Zonda staff in Hugel, for consistency with the other Schwarzwald Kafras. [6dc41a0077]
- **NPCs**: Added three custom Kafras to Hugel. (extension of the Origins Kafra QoL changes) [4c160b4748]
- **Items**: Added a disclaimer to the item description of `Mimic Card` to state that it doesn't stack in any way. [C:d04907cdd5]
- **WoE**: Added some missing items to the blacklist for pre-trans WoE. [Click her for the up to date blacklisted items](https://gitlab.com/originsro/originsro/-/blob/master/changelogs/attachments/pre-trans-woe-item-blacklist.md) [1448c3dc85]

#### Changed

- **Quests**: Relocated the Hunter jobchange quest to Hugel. [1ddcfbd00f]
- **Quests**: Restored the Hugel parts of the Blacksmith quest. [6fdf57e9ac]
- **NPCs**: Enabled elemental armors at the Comodo Gambling. [513d6204e7]
- **Items**: Enabled Gremlin Card and Beholder Card. [96beb78b37]
- **Quests**: Made it possible to learn Create Converter before Elemental Change in all cases. [c52fe2a330]
- **Misc**: Made it no longer possible to set up buying stores with prices higher than the specified price limit. Related to originsro/originsro#1986 [58c3bc5cf1, 13c56b5c16, 3008d5e213]
- **Skills**, **Items**: Disabled Polymorph (Hylozoist Card autocast skill) when the map doesn't allow the usage of Dead Branches. Related to originsro/originsro#2865 [3394c2c65f]
- **Maps**: Disabled Dead Branches on cmd_fild07. [be901a38eb]
- **WoE**, **Skills**: The following changes have been made to pre-trans WoE only: [ec650e463a, f0ad95bdb0, 678e0baa9e, 0efd895c4b, dbfb68a144]
  - Made Traps only triggerable by enemies. Related to originsro/originsro#2807
  - Made Quagmire affect only enemies. Related to originsro/originsro#2808
  - Made Snap always cost Spirit Spheres, even in Fury state. Related to originsro/originsro#2809
  - Made Snap stop on enemy traps. It is not possible to snap over them. Related to originsro/originsro#2810
  - Made Snap not useable when ankle-snared. Related to originsro/originsro#2810
- **NPCs**: Prevented entering Hugel Bingo with more than 1 character of the same person. [f7b43d8fb4]
- **NPCs**: Prevented entering one Hugel Monster Race with more than 1 character of the same person. [fec5037dc4]
- **Misc**: Improved several server-side operations that happen hundreds of times per minute by a factor of 4, in some cases by a factor of O(player_count). [b38811e481, f9b566341d, d1a2d3fe5d, a9cb97643e, 9c6d00dae8, 8f16113eed, cea3d71eb9, d7788797d2, 874153d0a2, 93ba5886b9, 44665caf4b]
- **Mobs**: All MVPs that spawn a tomb after dying will do so now at a static location for each map. Usually near a fequently used warp portal, in the center of the map or near its fixed spawn if it has one. Related to originsro/originsro#2420 [728cde84d5]
- **Items**: Changed the ingame name of Thin Blade to Curved Sword to match the Control Panel and database. Related to originsro/originsro#2368 [C:cc5434e650]
- **Misc**: Updated branding in the login screen.

#### Fixed

- **Quests**: Fixed getting stuck in Kiel Kyre Quest when leaving Mitchell's room too early. [f442f827e4]
- **Skills**: Fixed wrong bonus calculation when re-applying 1st trans spirit. Related to originsro/originsro#1881 [386b67d3aa]
- **Skills**: Prevent the monster skill Earthquake from dealing no damage when standing inside an icewall. Related to originsro/originsro#2744 [0ad70dd16b]
- **Skills**: Implemented official Lif base damage behavior for `Mental Charge`. Related to originsro/originsro#2010 [85de4e210a]
- **Skills**: Implemented official behavior for `Acid Demonstration` Armor & Weapon breaking. Mobs will no longer be affected by this. Related to originsro/originsro#2792 [fa4332802c]
- **Skills**: Improved range and cast time of `Soul Destroyer` to its official values. Related to originsro/originsro#1989 [0658db09e4, C:0b7f95b0e3]
- **Skills**: Improved `Fiber Lock` cast range to its official value. Related to originsro/originsro#2844 [f1d95a53e5]
- **Skills**: Improved `Eska`'s Def buff / debuff to its official behavior. It only affects the random part of vit def / soft def. Related to originsro/originsro#1884 [ec2ef1747a]
- **Misc**: Fixed incorrect movement speed, hp and sp when logging to an auto-trading character. Related to originsro/originsro#2429 [b7c10cf6f7]
- **Skills**: Implemented official reset behavior of Star Gladiator's `Miracle`. It won't reset on death anymore. Related to originsro/originsro#1992 [c3151a0f0a]
- **Skills**: Implemented official reset behavior of `Solar, Lunar and Stellar Courier`. It won't reset on death anymore. Related to originsro/originsro#2076 [090692aeac]
- **Skills**: Implemented official targetting for the guild skills `Battle Orders`, `Regeneration` and `Restoration`. They won't affect the guild master / caster. They will affect allied guilds as well. Related to originsro/originsro#2876 [02987864cf, fbfa44a4b3]
- **Skills**: Implemented official targetting for *guild skill auras*. They will only affect players. Related to originsro/originsro#2868 [4d22d7e2e3]
- **Mobs**: Fixed a rare case of monsters mysteriously becoming invincible. [5b10120965]
- **Items**: Prevented an exploit where you'd tame a monster without consuming the taming item. [d8bb0fa8e0, bb5015cdb6, 2a921a992f]
- **Skills**: Implemented the official chance and behavior to fail applying `Elemental Change`. [ac73457a8a]
- **Skills**: Implemented official targetting for `Soul Siphon`. It won't be possible to use it on monsters. [e065042a81]
- **Skills**: Implemented official `Final Strike` damage calculation. `Ice Pick`'s effect won't work with this skill. Related to originsro/originsro#1423 [09fd791c74]
- **NPCs**: Prevented an exploit that let you wager on Hugel Monster Race for the spectator fee. [22a1d3b07d]
- **Skills**: Corrected internal AttackTypes of 273 skills to their official values. **This may cause some skills to stop working** Related to originsro/originsro#2729, originsro/originsro#2731 [2c378f6e15, c6f1fd3e4f]
- **Skills**: Implemented official GTB behavior for self-cast only skills. [15654933af]
- **Skills**: Made Magnificat, Gloria and Angelus not work on yourself according to officials when using GTB and not being the one that casts it. [0d40c78539]
- **Mobs**, **Skills**: Fixed Monsters not ignoring characters under the influence of `Gangster Paradise` or `Trick Dead` when finding a random target. Related to originsro/originsro#2759, originsro/originsro#2896 [86c5036846, a21c822cd1]
- **Skills**: Imlemented official `Elemental Change` behavior in relation to the applied element's level. It is no longer random. [a5804d5742]
- **Skills**: Corrected the range in the skill description of `Shield Boomerang`. Related to originsro/originsro#2836 [C:a589320d64]
- **Items**: Corrected the item description of `Armor Charm`. Related to originsro/originsro#2163 [C:5cc30af2f3]

> This update consists of 148 commits.
