## Update Changelog - 2021-04-25 (Ep. 10.3 supplementary update)

> The maintenance also removed some server-side load.

### Changelog for Players

#### Added
- **Mapflags**: Added new no-vending areas for Einbroch, Ayothaya and Comodo. [ca4967ce4e, 1ec35f4c4e]
- **Mobs**: Implemented a mechanic to prevent leaving non-boss monsters damaged in order to stop griefing. They get full-healed and their exp distribution resets after being idle for some duration. [43328bdf76]

#### Changed
- **Items**: Disabled using delayed consumeables while storage open. This prevents an exploit of not using up the item but gaining its effect. Related to originsro/originsro#2782 [089bb0b02b, ff8b7f9a]
- **Events**: Disabled the easter event. [06760936c5]

#### Fixed
- **Skills**: Fixed Kihop counting the Taekwon itself onto the bonus on some cases. [d4bd0a632c]
- **NPCs**: Fixed a possibility to have more than 1 instance of the Pickpocket Quest NPC unhidden at the same time. Related to originsro/originsro#2742 [a95173c1fb]
- **Skills**: Fixed Hindsight not updating the selected skill when recasting. Related to originsro/originsro#2730 [fc9e26adc4]
- **Skills**: Fixed Hocus Pocus becoming unable to use, when rolling Warp Portal. [3243fee7d9]

> This update consists of 18 commits.
