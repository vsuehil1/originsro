# Update Changelog - 2020-09-06 (Ep. 10.3)

#### Known Issues
- **Skills**: The skill Focused Arrow Strike covers a too wide path, especially when the target is in a diagonal direction.
  - A fix will be made available in a future update.
- **Skills**: The skills Frost Joker and Dazzler don't use the custom messages from `ba_frostjoke.txt` and `dc_scream.txt` of `custom.grf`.
  - The feature has been temporarily disabled for compatibility with `@nofjoke`. The functionality will be restored in an upcoming update.
- **Misc**: The Wedding Dress and Tuxedo dyes don't match the hue of their respective regular dyes.
  - A fix will be made available in a future update.
- **Misc**: A few Knight dyes don't have a color for the peco (showing as solid black).
  - A fix will be made available in a future update.

#### Added
- **Commands**: Implemented `@partyinfo`. originsro/originsro#658
- **Features**: Enabled the Bank system, allowing to store money in a centralized place for an entire Master Account. originsro/originsro#360
  - In order to obtain a bank account, a quest is available at the Lighthalzen Bank.
- **Quests**: Added the Thanatos Tower quest and the relevant maps.
  - Thanatos Tower Entrance (`hu_fild01`)
  - Thanatos Tower Dungeon
- **Costumes**: Added 17 new costumes.
  - Red Bonnet Costume
  - Eye Patch Costume
  - Geek Glasses Costume
  - Big Ribbon Costume
  - Monk Hat Costume
  - Doctor Band Costume
  - Halo Costume
  - Mr. Scream Costume
  - Welding Mask Costume
  - Grand Circlet Costume
  - Fin Helm Costume
  - Spore Hat Costume
  - Indian Headband Costume
  - Goblin Leader Mask Costume
  - Angry Snarl Costume
  - Censor Bar Costume
  - Hahoe Mask Costume
- **Features**: Enabled the mail system. It can be accessed through the mailboxes in the major towns, the party and friend lists and the `@mail` command. Note: for security reasons, only text messages can be sent; items and money cannot be attached to a mail message. originsro/originsro#781
- **NPCs**: Added an Event Rewards Clerk NPC in the Aldebaran Kafra Headquarter, to hand out event rewards to eligible players. When a reward is available, a notification will appear in the chat window when logging in.
- **Commands**: Added the `@nofjoke` command, to disable display of the Frost Joker / Scream random messages. Note: for technical reasons, any custom `ba_frostjoke.txt` and `dc_scream.txt` entries are currently ignored. The customization functionality will be re-enabled in a future update.
- **UI**: Added the currently connected character name to the client titlebar. originsro/originsro#699
- **UI**: Added the destination map name identifier to the Warp Portal selection window. originsro/originsro#1092

#### Changed
- **Experience**: Limited out of party tapping to two taps. Party taps will stay unchanged (up to 11 taps)
- **Experience**: Capped the single-client party EXP bonus to 8 members.
- **Experience**: Disabled the single-client party EXP bonus in `lhz_dun03`.
- **Misc** Allowed chat messages starting with '`@`' so long as the second character isn't a letter.
- **Commands**: Improved `@droploot` to have multiple commands per line e.g. `@droploot +id1 +id2 +id3`. originsro/originsro#1255
- **Skills**: Implemented the official behavior of Earth Spike Scroll when Happy Break is active, to consume 10 SP and have a chance not to consume the scroll.
- **Misc**: Performance and security improvements.
- **Mobs**: Improved the usability of the Amon Ra map by making its summons disappear after 10 minutes. originsro/originsro#459
- **Misc**: Improved compatibility with unicode international symbols.
- **UI**: Made the order of the guild expulsion list persist after a server reboot (note: this only affects expulsions that occur after this update).
- **Commands**: Added a rate limiter to prevent spam of atcommands. Normal command use is not affected by this.
- **NPCs**: Relocated the Brave Warrior NPC near the Knight Guild, to make room for the Mailbox NPC.
- **Misc**: 147 new/modified clothing dyes, replacing dyes 141-287. Additionally, dyes 343-369 are being removed. If your character is currently wearing one of these palettes, you will automatically be given a free clothing coupon that you can redeem at the tailor NPC in southwest Prontera (refer to this [list](attachments/dye-replacements.md) to find candidate replacements). Please keep in mind that your dye may have been altered or removed, and even if it was not, the number will have shifted and you will need to purchase it again. Note: This will not affect any non-expanded first class characters or novices.
- **UI**: Updated the message that appears when the client crashes o remove some incorrect or misleading references.
- **Sound**: Increased the maximum sound bitrate reproducible by the client to 44.1kHz. This improves the sound quality when playing recent official BGM tracks as well as third party BGM remixes. originsro/originsro#1439
- **Skills**: Improved client performance when using the Sight Trasher or Cart Revolution skills against several targets.
- **UI**: Improved alignment and legibility of the numbers (item quantities) in the shortcut bars.
- **UI**: Changed the guild member list sorting to be by guild title (position), grouping the online members at the top. originsro/originsro#716
- **UI**: Improved the Storage search case handling. When searching a lowercase word, the search is now (again) performed case insensitively, while it is case sensitive when searching for a word that includes uppercase letters. originsro/originsro#1736
- **Misc**: Improved the autofollowing logic behavior to update the destination (and backtrack is necessary) when the followed target gets hit.

#### Fixed
- **Quests**: Fixed the Piano Key quest showing up in the CP as incomplete even after completing it.
- **Skills**: Fixed traps to ignore dead and out of (shootable) range targets.  originsro/originsro#666
- **Skills**: Fixed Grand Cross's shield strip removing player shield on map change. originsro/originsro#1215
- **Skills**: Fixed defense-bypassing skills incorrectly being reduced by soft MDEF. originsro/originsro#1110
  - Skills affected: Heal, Resurrection, Aspersio, Benedictio Sanctissimi Sacramenti, Sanctuary, Turn Undead, Fire Pillar
- **Skills**: Fixed Grand Cross inconsistently dealing 4 hits to caster. originsro/originsro#521
- **Skills**: Fixed Asura Strike's after-cast animation to correctly show the "fist" animation instead of standing still.
- **Skills**: Fixed Asura Strike so that it no longer makes the caster change their facing direction after casting
- **Skills**: Corrected the position of a character after a failed Asura Strike cast.
- **Skill**: Fixed Foresight so that it doesn't apply to item skills (e.g. fly wing). originsro/originsro#1686
- **Skill**: Fixed merchant's Item Appraisal SP cost.  originsro/originsro#1035
- **Skill**: Fixed Quagmire's ability to refresh the duration of its debuff. originsro/originsro#957
- **Items**: Fixed certain issues with delayed-consume items. originsro/originsro#1004
  - Fixed items being consumed when cast under cooldowns. originsro/originsro#1004
  - Using an item from beyond its range (e.g. Ygg Leaf) now correctly moves the player to the target and cast (whereas previously it'd get you in range and fail, while taking your item)
  - It's now possible to use consumable items while a scroll's cast cursor is visible, without cancelling the scroll use.
- **Party**: Fixed a bug with party/family share failing to recalculate the party's state before attempting share. originsro/originsro#1414
- **Party**: Fixed a bug with party member online status.
- **AI**: Prevented monsters from targeting an invisible (hiding) target when they're ineligible from doing so. originsro/originsro#777
- **Misc**: Fixed a bug with autotrade status persisting when kicked and disconnecting character when the 7 day period ends.
- **Misc**: Fixed some cases where dead characters were counted while they shouldn't.
  - Dead characters are no longer considered obstacles by walk logic.
  - Grand Cross will no longer count dead characters against the hit count.
- **Misc**: Improved deletion of pet eggs when they're on the floor.
- **Misc**: Fixed teleport destination when warping into a castle using WoE flags
- **Misc**: Fixed Bleeding attempting to stop characters moving and causing problems with position sync.
- **Skills**: Fixed a duplicate "Skill Failed" message when using Asura Strike.
- **Items**: Fixed the status icon from Elemental Resistance Potions not disappearing when their effect ends.
- **Skills**: Fixed Status Recovery to only cure status effects to targets that aren't Undead (such as Evil Druid wearers). originsro/originsro#866
- **Skills**: Fixed the behavior of scrolls and Hocus Pocus when the skill requirements aren't met.
- **Items**: Fixed a bug allowing to equip bullets and grenades regardless of the weapon type.
- **Skills**: Fixed the width of the path affected by Focused Arrow Strike to be 1 cell wide instead of 2 on each side.
- **NPCs**: Fixed an issue that caused loss of items when selling items to an NPC fails because of the character zeny cap.
- **Skills**: Fixed an issue that could cause the status icon timers to disappear on login or map change.
- **Skills**: Fixed Mild Wind level 7 getting dispelled when removing the shield. originsro/originsro#950
- **Commands**: Fixed an issue that caused `@ii` to fail under certain rare conditions. originsro/originsro#1234
- **Skills**: Fixed an issue that caused duplicate entries to be shown in the selection list when forging items. originsro/originsro#1398
- **Skills**: Fixed an issue that caused Adrenaline Rush to be removed when switching between items of the same type, under certain conditions. originsro/originsro#1575
- **CP**: Fixed an issue that caused some job-restricted items to show up as usable by every job.
- **Skills**: Fixed an issue that could allow a malicious user to access the training grounds with a non-novice character or leave the training grounds with unauthorized items. originsro/originsro#2192
- **Misc**: Fixed a position glitch when a character is killed by a script (such as in the Ayothaya Dungeon pits).
- **Skills**: Corrected the required level of Increase HP Recovery in the Concentration skill description.
- **Maps**: Improved the desert sound effects in the Morocc town map. originsro/originsro#1107
- **Skills**: Corrected the rage and cast time in the Soul Siphon skill description. originsro/originsro#1845
- **UI**: Fixed the Guild Message form in the Guild Window to support multi-line messages. Messages spanning across multiple lines will no longer be lumped together.
- **Skills**: Fixed a visual glitch (position desynchronization) when a Fire Pillar is pushed back.
- **Skills**: Fixed the display of the status icons when the character is affected by the Blind status effect.
- **Commands**: Fixed the use of the commands `/organize`, `/invite` and `/guildinvite` with names with spaces.

#### Removed

- **Events**: Removed Summer Event maps and NPCs.
  - The Ticket Trader will remain in Prontera to exchange any leftover Summer Festival Tickets for hats until 30th September.

> This update contains 1100 commits

## Update Changelog - 2020-09-28 (Ep. 10.3 supplementary update)

#### Changed

- **Maps**: Re-enabled the single-client party EXP bonus in `lhz_dun03`, but with a 50% reduction. This is an experimental change and it may change in the future, based on observation and feedback.
- **Maps**: Modified the Thanatos Tower quest to send every character back to the entrance of the tower instead of the 12th floor after the MVP dies, in order to prevent a repeatability abuse.
- **Quests**: Enforced the Thanatos Tower quest item requirements on every party entering the boss room. This requires each party that wants to take part into the boss fight to gather all the required items each time. All the Charm Stones must be used and all the Fragments must be on the first party member that enters the boss room, granting access to the rest of the party.
- **Maps**: Added a restriction to party actions (create, join, leave, invite, kick) on the 12th floor of the Thanatos Tower, to prevent unexpected situations that would cause a player to get stuck on that floor and unable to progress to the boss room.
- **Maps**: Disabled the use of Ice Wall in the Thanatos Tower - Upper Stairs map, to prevent abusive behaviors.
- **Misc**: Re-scaled the single-client party EXP bonus again to cover 12-player parties. This is an experimental change and it may change in the future, based on observation and feedback. See the following table for details of the change.
  - Previous settings:
    - For parties of two or more unique members: 10% flat bonus
    - Bonus for each unique member (from the 2nd to the 8th): +15%
  - New settings:
    - For parties of two or more unique members: 10% flat bonus
    - Bonus for each unique member (from the 2nd to the 5th): +15%
    - Bonus for each unique member (from the 6th to the 8th): +10%
    - Bonus for each unique member (from the 9th to the 12th): +5%

| Party size | Previous             | New                                     |
| ---------- | -------------------- | --------------------------------------- |
| 1          | 0%                   | 0%                                      |
| 2          | 10% + 1 * 15% = 25%  | 10% + 1 * 15% = 25%                     |
| 3          | 10% + 2 * 15% = 40%  | 10% + 2 * 15% = 40%                     |
| 4          | 10% + 3 * 15% = 55%  | 10% + 3 * 15% = 55%                     |
| 5          | 10% + 4 * 15% = 70%  | 10% + 4 * 15% = 70%                     |
| 6          | 10% + 5 * 15% = 85%  | 10% + 4 * 15% + 1 * 10% = 80%           |
| 7          | 10% + 6 * 15% = 100% | 10% + 4 * 15% + 2 * 10% = 90%           |
| 8          | 10% + 7 * 15% = 115% | 10% + 4 * 15% + 3 * 10% = 100%          |
| 9          | 10% + 7 * 15% = 115% | 10% + 4 * 15% + 3 * 10% + 1 * 5% = 105% |
| 10         | 10% + 7 * 15% = 115% | 10% + 4 * 15% + 3 * 10% + 2 * 5% = 110% |
| 11         | 10% + 7 * 15% = 115% | 10% + 4 * 15% + 3 * 10% + 3 * 5% = 115% |
| 12         | 10% + 7 * 15% = 115% | 10% + 4 * 15% + 3 * 10% + 4 * 5% = 120% |

#### Fixed

- **UI**: Fixed issues with party icons in the mini-map not updating properly on teleport or failing to show up at all for dead party members.
- **Commands**: Fixed the error "You're typing commands too fast" incorrectly appearing every time the `@cl` command is used.
- **Skills**: Fixed an issue that could allow a player that logs in on top of an Ice Wall cell to be able to bypass the teleport restrictions of the current map.
- **Misc**: Fixed an issue that caused observable position-lag glitches in various situation.
- **Skills**: Fixed an issue that caused monsters blocked by an Ice Wall to cast skills at a higher frequency rate than they should.
- **Skills**: Fixed an issue that caused monsters to act erratically under certain conditions, after using their reaction skills (retaliation or rude attack).
- **UI**: Fixed an issue that caused visual glitches (empty inventory and equipment) when logging in on top of a warp (i.e. by logging out as cloaked).
- **Items**: Fixed a missing 'Beloved' prefix on pet eggs.
- **Skills**: Fixed an issue that prevented the Bard and Dancer Spirit from granting the Dancer skills to a Bard or Minstrel.
- **Maps**: Fixed the character level restriction to access the upper levels of the Thanatos Tower.
- **Misc**: Fixed the error "You're typing commands too fast" unexpectedly appearing when using the `/hi` command or sending chat messages.
- **Skills**: Fixed an issue that caused any skills cast through Hocus Pocus to enfore their regular requirements, rendering most of them unusable by a Sage. originsro/originsro#2227
- **Skills**: Fixed an issue that caused Yggdrasil Leaf's Resurrection to require a Blue Gemstone. originsro/originsro#2240
- **Skills**: Fixed an issue that caused the Quagmire de-buff to remain active after walking out of the affected area, in certain cases.
- **UI**: Fixed a duplicate party state and equip window state message on login.
- **Skills**: Fixed an issue that caused Land Protector to block the Star Gladiator's Warmth skills.
- **Skills**: Temporarily reverted the recent change to Focused Arrow Strike that caused its area of effect to be narrower than expected. This will be re-applied in a future update, once improved to behave as intended.
- **UI**: Fixed an issue that caused the Service Selection screen to label the game server as `[New]` after each maintenance.
- **UI**: Fixed a client crash when sending a mail message with a long subject line.

> This update contains 47 commits

## Update Changelog - 2020-10-31 (Ep. 10.3 supplementary update)

#### Added

- **Events**: Added The Lost Manor (Halloween 2020) Event. See https://originsro.org/event/halloween-2020 for details.

#### Changed

- **Misc**: 59 modified/improved clothing dyes, replacing dyes 288-319 and 320-346. Additionally, dyes 320-328 are being removed. If your character is currently wearing one of these palettes, you will automatically be given a free clothing coupon that you can redeem at the tailor NPC in southwest Prontera (refer to this list to find candidate replacements). Note: This will not affect any non-expanded first class characters or novices.
- **Anticheat**: Improved and taken out of beta the AI-powered Origins Anti-Cheat system, after extensive testing. Note: in order to avoid unjust punishment in case of false positives (which may always happen when using an AI), the system operates on the server side and analyzes the behavior of online characters (including but not limited to skill usage, item consumption, reaction to various situations), comparing with models from past cases of fair use and of cheating, then submits a report to a GM. No punishments will be handed out until the case has been validated by a human. Note: for performance testing reasons, the system won't operate at full capability until November 27th (end of the Halloween event)
- **Client**: Improved the camera position restoration after visiting a map with a forced camera position (Juperos, Thanatos, etc).

#### Fixed

- **NPCs**: Fixed Kafra Pass related dialogues in the Kafra NPCs and fixed a possible exploit.
- **Misc**: Fixed some rare issues that could prevent the bank vault from loading properly.
- **Skills**: Fixed a bug that would cause multiple instances of Gospel to stack and cancel each others.
- **Misc**: Fixed a rare failure condition that could occur in map-wide warps (such as during WoE or in quests).
- **NPC**: Fixed an issue in the Lutie wedding NPC that could allow a character to get stuck while attempting to marry himself or herself.
- **NPC**: Fixed an issue that would allow a costume item exchanged through the Costume Exchanger NPC to retain its cards (despite the warning that they would be lost). Any such cards have been found and removed to match what the NPC should have done.
- **Skills**: Fixed Plagiarism's inability to copy skills for which the character has already learned all the requirements (allowing for example a Rogue to copy Double Strafe if she or he has Vulture's Eye level 10). originsro#1447 originsro#2181
- **Client**: Fixed a rare client crash when other (non-Origins) clients are open at the same time.
- **Client**: Fixed a possible client crash when entering in the view range of an Eclipse pet.
- **Client**: Fixed a missing URL in the screenshot watermark.
- **Client**: Fixed various issues with hairstyles, weapon, shields and headgears when replaying .rrf files in Replay Theater mode.
- **Client**: Fixed missing skill units (such as Ice Wall, Land Protector, Quagmire, etc) when replaying .rrf files in Replay Theater mode.

#### Removed

- **Events**: Removed the Summer 2020 event trader NPC.

> This update consists of 162 commits

## Update Changelog - 2020-12-23 (Ep. 10.3 supplementary update)

### Changelog for Players

#### Added

- **Events**: Added the Christmas 2020 Event contents.
- **WoE**: Added the pre-trans WoE tryout event. Pre-trans WoE will run on Saturday, January 9th at 19:00 UTC in the Holy Shadow (North-East Greenwood Lake, Payon 3) castle, with the following rules:
  - Transcendent classes as well as Ninja, Gunslinger, Star Gladiator and Soul Linker won't be able to access the castle.
  - Transcendent class buffs will be removed when entering the castle. This includes Full Chemical Protection (but the protections won't be removed if applied individually), Assumptio, Wind Walker, Mind Breaker, any Gospel buffs, the Soul Linker Ka- buffs, Spirits and derived buffs (Advanced Adrenaline Rush, One-Hand Quicken, Berserk Potion Pitcher, Super Novice disallowed equipment, etc).
  - Ankle Snares will use the pre-trans mechanics, immediately stopping the movement of the character that triggers them.
  - Homunculi will not be able to access the castle.
  - Ranker Taekwons will have their HP and SP bonus reduced to 1.5x instead of 3x.
- **NPCs**: Added homunculus and pet rename services to the Civil Registry Clerk NPC.
- **Misc**: Implemented seamless autotrade reconnection, allowing to log back into an autotrader without kicking the character offline. This is an experimental feature and currently only works during a short time after using the `@autotrade` command. In the future it'll be extended to work through the entire duration of the autotrade.
- **Items**: Added the loyalty level to pet eggs name in the inventory.
- **Items**: Added the summoner's name under mobs summoned by a player (through Dead Branch or other means). originsro/originsro#2191
- **Mobs**: Added MVP damage log window, available to party members of those that took part in the killing as well as to whoever was within sight range when the MVP died. The log can be opened by double clicking the buff icon that will appear on the screen during the 60 seconds after the death of the MVP.
- **Commands**: Added an `@hidebubbles` (alias: `@hidestorechat`) command to toggle the display of the vending/buying store balloons. originsro/originsro#2337
- **Chat**: Added a `#pvp` chat channel.
- **UI**: Added delay "clock effect" indicator to the homunculus skill icons.
- **UI**: Added an Options menu in the item description window, allowing to easily access the `@alootid`, `@droploot`, `@iteminfo`, `@whobuys`, `@whosells` commands on that item, insert the item ID in the chat or open the corresponding page in the Control Panel. Players that wish to update their custom skins to override the button's appearance will need to create a blank button shape texture of `77x20 px` in their skin directory called `btn_oro_options.bmp` and `btn_oro_options_a.bmp` for the normal and hovered/clicked states respectively.
- **UI**: Added a confirmation prompt when attempting to npc-sell any carded, refined or unidentified items.
- **UI**: Added a right-click menu on monsters (that can be disabled with the `/rclick` command to avoid accidentally triggering it) to easily access the `@mobinfo` command, insert the mob ID in the chat or open the corresponding page in the Control Panel.
- **UI**: Added the current amount of items to the Inventory window title.

#### Changed

- **Mobs**: Changed the Outlaws to be targettable without holding Shift.
- **UI**: Added the remaining jail time to the login messages for jailed characters.
- **Items**: Prevented dropped items from falling on top of MVP tombs and becoming impossible to pick up.
- **Commands**: Extended `@killcount` to support multiple (up to 3) mob IDs. originsro/originsro#424
- **Skills**: Changed Lex Aetherna to bypass `@noks`. originsro/originsro#2003
- **Commands**: Added a new toggle syntax (using the `~` symbol) to the `@alootid` and `@droploot` commands, useful when it's desirable to have a single shortcut to toggle on and off an item from the list.
- **Skills**: Changed Chemical Weapon/Helm/Armor/Shield extend the remaining time even when cast at a lower level. originsro/originsro#1999
- **NPCs**: Improved the visibility of the PvP NPC when there are players inside.
- **Commands**: Increased the amount of autocommands available through `NPC:AutoCommands` to 15.
- **Items**: Moved the pet equip to the Pet tab in the Storage, Guild Storage and Master Storage.
- **Items**: Moved arrow quivers and bullet cases to the Ammo tab in the Storage, Guild Storage and Master Storage.
- **UI**: Removed pointless Service Selection screen on login, showing only one option.
- **UI**: Improved the text wrapping (increased the width, removing the excessive blank space) in the item description window.
- **UI**: Changed the Vending setup window to remember and automatically fill the last used set of items (as long as they are still in the cart). A reset button has been added, to start from a clean window.
- **UI**: Improved the prices formatting in the Vending setup window, automatically showing thousands separators once the editing of each price field is complete, to avoid mistakes.

#### Fixed

- **NPCs**: Fixed a zeny exploit in the Tailor NPC (this had already been applied as a hotfix before this update)
- **Quests**: Fixed an issue that could allow to spawn monsters more than once in the Thanatos Tower quest.
- **Misc**: Fixed an issue that could allow an unauthorized guild leader change through a forged packet.
- **Misc**: Fixed an issue that could allow a banned or kicked character to perform further operations before getting kicked offline.
- **Skills**: Fixed Deluge, Violent Gale and Volcano adding a damage bonus that bypasses elemental resistance. originsro/originsro#2302
- **Skills**: Fixed a position bug caused by Parry. originsro/originsro#2270
- **Commands**: Fixed a typo in the `@memo` command.
- **Skills**: Fixed Stone Curse to trigger on targets regardless of their element. originsro/originsro#1873
- **Skills**: Fixed Fiberlock getting destroyed by a friendly Magnum Break on the trapped character. originsro/originsro#1856
- **Quests**: Fixed a rare condition that could render the Sign quest unfinishable for everyone.
- **Quests**: Fixed an issue that could allow multiple people to enter the solo room of the Sign quest at the same time.
- **Homunculi**: Fixed homunculi not warping back to their master in time.
- **Pets**: Fixed pets getting separated from their master.
- **Misc**: Internal bug fixes and feature improvements.
- **Misc**: Fixed a client crash when certain files are missing.
- **Sound**: Fixed a fade-out/cutoff effect in the BGM when teleporting within the same map.
- **Misc**: Fixed some crashes caused by nVidia graphics drivers on Windows 10. This can be disabled in case it causes issues with other graphics drivers through the `/crashfix` command. WINE (Linux and macOS) systems are unaffected by this.
- **UI**: Fixed the item description window not always remembering the position it was dragged into.
- **UI**: Improved the position of the title of the Inventory window to be more consistent.
- **UI**: Fixed the position of the item pick up notification bubble so that it doesn't overlap with broadcast messages.
- **UI**: Fixed and re-enabledthe storage filter on the Pet tab (disabled in the previous update due to a client crash).
- **Misc**: Fixed an issue that could cause the guild emblem to not show up properly under your own character.

> This update consists of 109 commits
