# Update Changelog - 2020-04-28 (part of Ep. 10.2)

## Changelog for Players

### Added

- **NPCs**: Added new Kafra Employees to the following locations:
  - 2 in Alberta (for a total of 4)
  - 3 in Aldebaran (for a total of 4)
  - 2 in Amatsu (for a total of 3)
  - 2 in Ayothaya (for a total of 3)
  - 2 in Comodo (for a total of 3)
  - 1 in Einbech (for a total of 2)
  - 1 in Einbroch (for a total of 3)
  - 2 in Geffen (for a total of 4)
  - 2 in Kunlun (for a total of 3)
  - 1 in Izlude (for a total of 2)
  - 1 in Lighthalzen (for a total of 3 + 1 inn Kafra)
  - 2 in Luoyang (for a total of 3)
  - 2 in Morocc (for a total of 4)
  - 2 in Payon (for a total of 4)
  - 0 in Prontera (for a total of 6)
  - 2 in St. Capitolina Abbey (for a total of 2)
  - 1 in Lutie (for a total of 1)
  - 1 in Juno (for a total of 4)
  - 2 in Umbala (for a total of 3)
- **NPCs**: Added the following Kafra Warps:
  - St. Capitolina Abbey -> Prontera
  - Lutie -> Aldebaran
  - Einbroch -> Juno
  - Lighthalzen -> Juno
- **Skills**: Added a status icon for the Soul Linker Spirit buffs.
- **Skills**: Added a status icon for Solar, Lunar and Stellar Union.
- **Items**: Added status icons for the Elemental Converters.
- **Items**: Added a status icon for Cursed Water.
- **Commands**: Added a new `/mineffect2` command, applying the following WoE-oriented optimizations (the command, like /mineffect, is automatically enabled in WoE, but can be manually enabled or disabled at any time). The following effects are simplified:
  - Soul Link (removed overhead letters)
  - Jupitel Thunder (simplified animation)
  - Blessing (simplified animation)
  - Increase Agility (simplified animation)
  - Decrease Agility (simplified animation)
  - Kaupe (simplified animation)
  - Tumbling (simplfied animation)
  - Flip Tatami (simplified animation)
  - True Sight (simplfied animation)
  - Various hit and cast effects (removed or simplified)
  - Auto Guard/Kyrie Eleison (improved performance)
- **Commands**: Added a new `/cpubg` command, reducing CPU usage of background clients (note: the background CPU usage reduction from the launcher must be DISABLED in order to enable this). The command's state is saved on logout. Please report any issues you may encounter with it.
- **Commands**: Added a new `/fpsboost` command, to improve the client's FPS in busy situations (such as WoE). The command's state is saved on logout. Please report any issues you may encounter with it.
- **UI**: Updated the World Map image with recognizable landmarks for the main towns and with the dungeon names (by pressing Tab to switch to the alternate map). Many thanks to ElNinoFr for preparing and tweaking it for us!
- **UI**: Added new loading screens, based on artwork submitted for the Soul Linker/Star Gladiator Art Event by the following players (thank you!):
  - 321
  - Blissilia
  - ExCharny
  - Judar
  - k.
  - Kido
  - Kithira
  - mina-i
  - Muskrat
  - Purrincess
  - Salvi
  - Shirohana
  - Sonschi
  - Star-linked
  - Xellie
- **UI**: Updated the minimap icons to reflect the locations of Kafra employees, shops and guides.

#### Changed

- **Misc**: Extended the Guest permission "Allow Character Build" to also control adoption permissions.
- **Misc**: Extended the Guest permission "Allow Character Build" to also control divorce permissions.
- **Misc**: Reduced the slots available for character creation to 3. Characters in slots 4 through 9 will still be available to log in without limitations.
- **Quests**: Changed the order of preference of the Bard Job Change quest rewards, when materials for more than one are available. Harp/Lutie will be automatically picked over Mandolin, or Mandolin over Violin. originsro/originsro#1952
- **Skills**: Changed Magnificat, Aspersio, B.S. Sacramenti, Gloria and Assumptio to allow to extend (and never shorten) the duration of an existing buff regardless of its level. originsro/originsro#1888
- **Skills**: Changed Blessing and Increase Agility to allow to extend (and never shorten) the duration of an existing buff of the same level. originsro/originsro#1957
  - The 1-minute buffs from Gospel will no longer override the priest level 10 ones (unless they have less than one minute remaining)
  - The 4-minute buffs from a priest will no longer override the TK class level up buffs (unless they have less than four minutes remaining)
- **Commands**: Changed the `@gsacl` command to prevent a guild leader from locking themselves out of their own storage. Guild leaders are now always allowed to access their guild's storage.
- **UI**: Added a decimal digit to the ASPD value in the status window.
- **Commands**: Extended the `/mineffect` command to minimize the following additional effects (note: the Simplfied Visual Effects option in the launcher must be enabled for optimal results):
  - Impositio Manus (minimized visual effect)
  - Lord of Vermillion (simplified visual effect and greatly increased transparency)
  - Meteor Storm (simplified visual effect and greatly increased transparency)
  - Quagmire (simplified visual effect and greatly increased transparency)
  - Devotion (simplified visual effect)

#### Fixed

- **Items**: Fixed Hunter Fly Card triggering for Grand Cross while it shouldn't.
- **Homunculus**: Fixed an issue in the guest check for homunculus deletion.
- **Quests**: Fixed a minor issue in the Doctor quest.
- **Misc**: Fixed the cast bars and cast targets not showing up when entering the caster/target's sight range after the cast started.
- **Skills**: Fixed some issues with the Pneuma visual effect when leaving and re-entering its sight rage.
- **UI**: Fixed some issues in the storage sorting, when refined or carded items are present.
- **Items**: Fixed an incorrect interaction between Speed Potions and Decrease Agility, Quagmire, Please Don't Forget Me, causing GM-like speeds. originsro/originsro#1854
- **Skills**: Fixed an unintended interaction between Bard/Dancer songs and homunculi. Homunculi will no longer be affected by them. originsro/originsro#1325
- **Skills**: Fixed the Star Gladiator Miracle status not terminating when logging out.
- **Quests**: Fixed an issue causing the Cursed Spirit quest spirits not to trigger when another character is already standing on the trigger area. originsro/originsro#1949
- **UI**: Fixed the party level range used by the Party Booking System to match the game requirement (10 levels).
- **UI**: Fixed some missing locations in the Party Booking System.
- **Skills**: Fixed an issue causing the homunculus level-selectable skills in the hotkey bars to reset to their maximum level when logging in. originsro/originsro#1813
- **Commands**: Fixed the `/loading` command behaving the opposite way of what it was supposed to.

#### Deprecated

- **Settings**: The background CPU reduction setting in the launcher is now deprecated (superseded by the `/cpubg` command that can be toggled at runtime), and will be removed from the launcher in the future.

#### Removed

- **Events**: Removed the Easter event NPCs, Eclipses and eggs.

This update contains 62 commits.
