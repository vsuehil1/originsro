# 2020-07-19 (Ep. 10.2 patch 2)

#### Added

- **Events**: Added the Summer 2020 Event. See https://originsro.org/event/summer-2020 for details.
- **Items**: Added the missing color variants for costumes created from recolored hats (13 new costumes)
  - Brown Beanie Costume (200,000 Z)
  - Pink Beanie Costume (200,000 Z)
  - Brown Deviruchi Hat Costume (380,000 Z)
  - Gray Deviruchi Hat Costume (380,000 Z)
  - Red Deviruchi Hat Costume (380,000 Z)
  - Blue Drooping Cat Costume (500,000 Z)
  - Brown Drooping Cat Costume (500,000 Z)
  - Gray Drooping Cat Costume (500,000 Z)
  - Pink Drooping Cat Costume (500,000 Z)
  - Yellow Drooping Cat Costume (500,000 Z)
  - Brown Mage Hat Costume (500,000 Z)
  - Gray Mage Hat Costume (500,000 Z)
  - Yellow Mage Hat Costume (500,000 Z)
- **Misc**: 94 new/modified clothing dyes, replacing dyes 47-140. If your character is currently wearing one of these palettes, you will automatically be given a free clothing coupon that you can redeem at the tailor NPC in southwest Prontera (refer to this [list](attachments/dye-replacements.md) to find candidate replacements). Please keep in mind that your dye may have been altered or removed, and even if it was not, the number will have shifted and you will need to purchase it again. Note: This will not affect any non-expanded first class characters or novices.
- **Chat**: Added a chat filter for the `#ally` chat channel messages, to allow diverting them to a separate window or filtering them out.
- **Commands**: Added the command `@idsearch`, as an alternative to `@ii` with a more compact output.
- **Chat**: Added a filter for the `#party` and `#trade` chat channel messages, to allow diverting them to a separate window or filtering them out.
- **NPCs**: Added an extra Kafra Employee in Juno (now 5).
- **UI**: Added an `(U)` marker to the unidentified items in the Storage List and Storage Search, to make them easier to visually identify or search.
- **Commands**: Added a new command `/ballooncorners`, to control whether the chat message balloons should have rounded corners or the default sharp corners.

#### Changed

- **Misc**: Improved support for unicode characters.
- **Misc**: Performance improvements (both client and server side).
- **Skills**: Changed the stat transfer of the Undying Love skill to work the same regardless of gender.
- **Events**: Changed the Brave Warrior (Floating Rates event) donation requirement to 75M Zeny.
- **Misc**: Removed caret (`^`) from the symbols available for character creation or rename, since it caused glitches in NPC dialogues.
- **Misc**: Added the `ı`, `ş`, `ğ`, `İ` to the symbols available for character creation or rename. originsro/originsro#2118
- **Misc**: Improved the logging of guild storage transactions, to reduce the work necessary to research guild storage theft cases.
- **Items**: Renamed the etc Pocket Watch item to Kiel's Pocket Watch, to prevent mistakes in vending shops.
- **Items**: Updated the areas with vending restrictions in Prontera, Prontera Tool Dealer, Archer Village, Kunlun, Geffen, Aldebaran, Alberta and Payon.
- **UI**: Made the party member list entries clickable for their full width rather than just the character name.
- **UI**: Replaced the unused "Cash" tab of the Storage with a "Pet" tab for pet eggs.
- **Replay**: Improved the playback experience when replaying a sequence that contains teleports.

#### Fixed

- **UI**: Fixed a visual glitch in the chatrooms when the original owner leaves and transfers ownership.
- **Misc**: Fixed an issue in the character rename function.
- **Misc**: Fixed a possible error during character selection.
- **Misc**: Fixed a rare issue that could make the server lose track of whether an account has online characters and be unable to properly kick them when trying to log in again.
- **UI**: Fixed some cases where the HP bars of party members wouldn't update correctly.
- **Misc**: Fixed some (more) cases of Master Storage not working after going to the char selection screen and back to the game.
- **Skills**: Fixed the MDEF bonus of Eska.
- **NPCs**: Fixed an issue that would cause permanent loss of status points when an adopted character having stats over 80 uses the Physiotherapist NPC's services.
- **Quests**: Fixed some conditions that could cause Outlaws to stop appearing.
- **Misc**: Fixed an issue that could lead to the leak of a hidden GM's position in some rare cases.
- **Quests**: Fixed some issues, including a potentially exploitable one, in the Dokebi Battle Quest.
- **Skills**: Fixed an issue that caused ground skills to be still in effect when their caster is under a Basilica.
- **Skills**: Fixed the duration of Kaahi (350s).
- **Skills**: Fixed a visual glitch when casting Arrow Shower.
- **UI**: Fixed some visual issues in the shortcut bars with skills that aren't level-selectable.

#### Deprecated

- **NPCs**: Removed the female hair style 57 ("Miku hair") from the Hair Dresser NPC. The hairstyle wasn't well designed and was causing visual glitches and crashes (for example, when opening the skill window). In the future, it'll be completely removed and replaced by a new hairstyle.

#### Removed

- **UI**: Removed the small item parameter window that appeared under NPC shops and contained incorrect ATK/DEF information about the selected items. The item comparison window should be used instead, as it contains more accurate information.

> This update contains 244 commits.

# 2020-07-25 (Ep. 10.2 patch 2 supplementary update)

#### Changed

- **Events**: Improved the rewards of the ChamChamCham game of the Summer Event.
- **Events**: Changed 3 of the rooms of the Expedition game of the Summer Event to be reserved for parties only.
- **Events**: Improved the multi-player rewards of the Expedition game of the Summer Event.
- **Events**: Added a cooldown to the Expedition game of the Summer Event to prevent players from leaving in the middle of the game and entering another room.
- **Events**: Changed the food buffs of the Summer Event to allow players to purchase a new one immediately after dying, by visiting the NPC again.

#### Fixed

- **Misc**: Fixed an issue that could prevent enabling the non-multiclient party bonus in certain cases.
- **Misc**: Bug fixes for internal errors.
- **Maps**: Fixed an issue that could cause the PvP map counter to become negative.
- **UI**: Fixed the guild member mini-map icons not appearing when entering a map.
- **Events**: Fixed an issue that allowed homunculi to access the Expedition game of the Summer Event.
- **Events**: Fixed an issue that could permanently prevent access to the Expedition game of the Summer Event for an entire Master Account.
- **Items**: Fixed the weight in the description of the Summer Festival Ticket item.

> This update contains 48 commits.
