## Update Changelog - 2021-11-08 (Ep. 10.4 halloween & hotfix patch)

### Changelog for Players

#### Changed
- **Events**: Disabled Halloween Event. [7eca7a04dd]
- **NPCs**: Improved the UX of the Outlaw Reward NPC. Also prevent dropping rewards on the floor when they're too heavy or don't fit in the inventory. [d6ebc5f08f, a5b00bbcf2]

#### Fixed
- **Quests**: Fixed dialogue in the Kiel Hyre quest. [10b5e676bf]

> This update consists of 10 commits.
