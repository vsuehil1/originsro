## Update Changelog - 2021-12-24 (Ep. 10.4 christmas event)

### Changelog for Players

#### Added
- **Events**: Enabled the Christmas Event for 2021. See our Wiki. [16fa31463a, db8f8cd89c, 711766b185, 0dcf555340, d7d1c5ee3a, dbdd81d5a1, 12c24ad8fc, 62ede72972, b7fb992823, d6b5baa6ef, 98cec9725e, 6a39b6b15b, 93c91c5c48, 6686c56368, 1541910d69, 530e616d41, 212b61787e, 4358056829, 1af23c2bdc, a2f52b8698, 6eb6cfdbb1, 6bd8dc386b, 0c26ce29c8, 01527f7fb8, 240ebcb1c2, d2f7c1ebba, 4aebd1452e, fb63087c98, e86f831261, 3d82de30e2, b0344efc54, 9d91cdb58e, b7ead12f17, 22322499a3, ed10ed5e97, 6064a63cfa, c80ffcb6c4, c5c1f26f5b, 51119a567b, 864536752d, 0f7757199f, a7ae97c660, a8524dda7a, 54e6f724df, c742791d17, 07205e1200, 1a44d8ca5e, 80ca83b9d2, 6499f1615f, 42725eddd0, 5717bbb72a, bccb81a809, 54a3a545b7, c99bda3908, 8fd29113f5, 8105b4b84d, 307fbad16a, c1bff42569, 286054e032, 6dc46bbc9a, 417b8cb0bf, b440c081ad, 51c0d89514, 51d62ca8d5, C:527686605f, C:5065e6c06b, C:9e484482ca, C:8726b5712b, C:5e4f83350d, C:b77f1afcb6, C:954369ee80, C:cdb5798f20]
- **Other**: Added a requirement for the guild invitation permission for guests to leave a guild. [0b61e48e2b]
- **Other**: Added a requirement for the guild modification permission for guests to edit a guild member's position. [fd4bde61e1]
- **Other**: Added a requirement for the guild modification permission for guests to edit a guild position. [bacb693b66]
- **Other**: Added a requirement for the guild modification permission for guests to edit a guild emblem. [41659aac6a]
- **Other**: Added a requirement for the guild modification permission for guests to add skill points. [fd418aeea2]

#### Fixed
- **Other**: Prevented a bunch of exploits in relation to buying store and vending. Related to originsro#2604 [1e757c3585, 017a9e583c, 27f0fce2a8, 8ef0c461b1, 51f2cbd0a1, d956fc1122, 96bc5c8af5]
- **Skills**: Fixed Lord Knight skill `Concentration`'s effect and sound triggering on every map-change and teleport. [1e2444646c]

> This update consists of 132 commits.
