<!--
 ____ _____ ___  ____  _
/ ___|_   _/ _ \|  _ \| |
\___ \ | || | | | |_) | |
 ___) || || |_| |  __/|_|
|____/ |_| \___/|_|   (_)

For support request, please head to the OriginsRO forum or Discord, or to the Help Desk!

Help Desk: https://gitlab.com/originsro/originsro-support
Forum: https://bbs.originsro.org/
Discord: https://originsro.org/discord
-->
